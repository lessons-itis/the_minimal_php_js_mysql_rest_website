<?php
	
	session_start(); //prende le variabili della sessione aperta prima
	header('Content-type: text/html; charset=utf-8');
	
	//include 'session1.php';
	
	echo "Il mio nome è ".$_SESSION["name"].".<br>"; //scrivo il nome nel campo "name"
	echo "Il mio cognome è ".$_SESSION["surname"].".<br>"; //scrivo il cognome nel campo "surname"
	
	//print_r($_SESSION); per scrivere tutte le variabili senza usare echo
	
	// rimuovo le variabili della sessione
	session_unset();

	// elimino la sessione
	session_destroy();
?>