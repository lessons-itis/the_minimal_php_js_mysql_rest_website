<?php
  require "../../../header.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  
  $DBNAME = "esempio";
  $name=$_POST['name'];
  $username=$_POST['username'];
  $password=$_POST['password'];
  $confirm=$_POST['ConfirmPassword'];

  if($confirm != $password)
  {
    http_response_code(302);
	  echo "<p>Wrong password: try again</p>";
  }
  else
  {
 	session_start();

	$_SESSION["name"] = $name;
	$_SESSION["username"] = $username;
	$_SESSION["password"] = $password;


	require "reg_server.php";
  }

require "../../..footer.html";
?>




<section class="section">
    <h2>
    <?php
            echo "Registrazione";
    ?>
    </h2>


    <form class="container" action="" method="POST">
      <div class="field">
            <p class="control has-icons-left has-icons-right">
                <input class="input" name="name" type="text" placeholder="Name">
                <span class="icon is-small is-left">
                    <i class="fas fa-user"></i>
                </span>
            </p>
        </div>
        <div class="field">
            <p class="control has-icons-left has-icons-right">
                <input class="input" name="username" type="text" placeholder="Username">
                <span class="icon is-small is-left">
                    <i class="fas fa-user"></i>
                </span>
            </p>
        </div>
        <div class="field">
            <p class="control has-icons-left">
                <input class="input" name="password" type="password" placeholder="Password">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>

	    <p class="control has-icons-left">
                <input class="input" name="ConfirmPassword" type="password" placeholder="ConfirmPassword">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>		

        </div>
        <div class="field">
            <p class="control">
                <button class="button is-success">Registrati</button>
            </p>
        </div>
    </form>
</section>


