<?php
	
	session_start(); //prende le variabili della sessione aperta prima
	header('Content-type: text/html; charset=utf-8');
	
	//include 'session1.php';
	
	echo "Il mio nome è ".$_SESSION["nome"].".<br>"; //scrivo il nome nel campo "nome"
	echo "Il mio cognome è ".$_SESSION["cognome"].".<br>"; //scrivo il cognome nel campo "cognome"
	
	//print_r($_SESSION); per scrivere tutte le variabili senza usare echo (l'ho usato nella sfida 2)
	
	// rimuovo le variabili della sessione perché finita la sfida 1
	session_unset();

	// elimino la sessione della sfida 1
	session_destroy();
?>