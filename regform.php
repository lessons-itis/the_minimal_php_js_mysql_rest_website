<section class="section">
    <h2>
    <?php
            echo "Registrazione";
    ?>
    </h2>


    <form class="container" action="reg_server.php" method="POST">
      <div class="field">
            <p class="control has-icons-left has-icons-right">
                <input class="input" name="name" type="text" placeholder="Name">
                <span class="icon is-small is-left">
                    <i class="fas fa-user"></i>
                </span>
            </p>
        </div>
        <div class="field">
            <p class="control has-icons-left has-icons-right">
                <input class="input" name="username" type="text" placeholder="Username">
                <span class="icon is-small is-left">
                    <i class="fas fa-user"></i>
                </span>
            </p>
        </div>
        <div class="field">
            <p class="control has-icons-left">
                <input class="input" name="password" type="password" placeholder="Password">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>

	    <p class="control has-icons-left">
                <input class="input" name="ConfirmPassword" type="password" placeholder="ConfirmPassword">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>		

        </div>
        <div class="field">
            <p class="control">
                <button class="button is-success">Registrati</button>
            </p>
        </div>
    </form>
</section>


